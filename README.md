# React Code Test

Create a quiz application to answer questions about React. Please fork this repository and email the solution to Ciarán.

## Setup 

Uses a standard Create React App setup. Node and NPM required.

* `npm install` to install dependencies.
* `npm start` to start the React application.
* `npm run backend` to start the "backend".

### TypeScript

Although we always use TypeScript, this project is set up to run plain JavaScript. If you would prefer to write in TypeScript you can run this: 

```
npm install --save typescript @types/node @types/react @types/react-dom @types/jest
```

And then convert all `.jsx` files to `.tsx`. 

## Instructions

Make a quiz that asks users a series of questions about React. There are three questions in total each with multiple possible answers. If a user gets a question right, they can move on to the next round. If they get one wrong, they have to go back to the start.

There is a simple backend to request the questions from at the start. `npm run backend` will start it. You can use the function `getQuestions()` in `src/utilities/api.js` to request the data. Please see `data.json` at the root of the project to see the return format. Note that you don't need to change anything in the backend in this challenge.

To save time, we have provided a starting point and some components already made. CSS has also been provided so you don't have to write that (unless you would like to change the appearance, then feel free). You just need to attach the write classes to the correct components and the CSS styling will be applied (see diagrams below).

Please feel free to use class components, hooks or whatever you like. Also you can add as many or as few components as you think is best.

## Screenshots and CSS Classes

The screenshots here show what we'd like you to make. The arrows and labels show what CSS classes to apply to what elements so you don't need to waste time writing CSS (unless you would prefer to!)

![Main Screenshot](/screenshots/1.png "Main")

![Correct Answer Screenshot](/screenshots/2.png "Correct Answer")

![Incorrect Answer Screenshot](/screenshots/3.png "Incorrect Answer")

At the end you can display the `<Finished />` component:

![Finish Screen Screenshot](/screenshots/4.png "Finish Screen")

## Question Data

An array of questions is provided each with the format below. The `answerId` points to which `optionId` is the correct answer.

```
{
    "question": "What can you pass to components?",
    "answerId": 1,
    "options": [
        {
            "id": 1,
            "label": "Props"
        },
        {
            "id": 2,
            "label": "State"
        }
    ]
}
```