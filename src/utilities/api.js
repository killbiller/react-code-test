/**
 * Request questions from the backend.
 * @returns Promise of the questions from "data.json".
 * 
 * @example 
 * // Logs the data:
 * getQuestions.then(rounds => console.log(rounds));
 * 
 * // Or:
 * console.log(await getQuestions());
 */
export const getQuestions = async () => {
    const response = await fetch('http://localhost:3456/rounds');
    return await response.json();
};
