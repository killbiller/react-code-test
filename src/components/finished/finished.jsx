import React from 'react';

/**
 * Message when the user gets every question right.
 * @returns Victory message.
 */
export const Finished = () => (
    <p className="finished">Well done, you completed the challenge.</p>
);
