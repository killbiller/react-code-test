import React, { useState } from 'react';
import './app.css';

// Use getQuestions() to request the data from the API.
import { getQuestions } from '../../utilities/api';

/**
 * On mount, have the component fetch the questions from the backend. After retrieving the questions, render one round 
 * at a time. Players can continue to the next round each time they get a question correct. Getting one wrong means they
 * can only return to the beginning again.
 * @returns Quiz application.
 */
export const App = () => {
    const [roundIndex] = useState(0);
    return (
        <div className="app">
            <div className="title">Question {roundIndex + 1}</div>
        </div>
    );
};
